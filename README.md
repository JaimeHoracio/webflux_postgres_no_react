# Webflux_postgres_no_react



## Project topic
Este proyecto es solamente como prueba de concepto, para probar las configuraciones necesarias al conectarse a la base de datos, etc.
El proyecto contiene una REST API con WebFlux lo que lo hace asincronico y no bloqueante.
La base de datos es Postgres pero bloqueante, por lo que hay que convertir el resultado a Mono y Flux.
Se utiliza JPA para utilizar el DAO que se crea automaticamente de Spring Data y realizar el CRUD.
Se utilza MapStruct y Lombok para simplificar el desarrollo.

```
go to workspace
git clone https://gitlab.com/JaimeHoracio/webflux_postgres_no_react.git
git branch //To see the branch
git ckeckout name_branch //To change branch
git add . //Add all files new and changed
git commit -m "some comment"
git push
```
