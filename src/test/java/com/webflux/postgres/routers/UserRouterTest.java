package com.webflux.postgres.routers;

import com.webflux.postgres.application.dto.UserDto;
import io.netty.handler.codec.http.HttpObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

@SpringBootTest
@AutoConfigureWebTestClient
class UserRouterTest {

    //@InjectMocks
    //UserHandler userHandler;

    @Autowired
    WebTestClient webTestClient;

    @BeforeEach
    void setUp() {

    }

    @Test
    void getUsers() {
        webTestClient
                .get()
                .uri("/api/users")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void saveUser() {
        webTestClient
                .post()
                .uri("/api/user")
                .exchange()
                .expectBody(UserDto.class);
    }
}