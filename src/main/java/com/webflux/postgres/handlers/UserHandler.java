package com.webflux.postgres.handlers;

import com.webflux.postgres.application.dto.UserDto;
import com.webflux.postgres.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserHandler {


    private final UserService userService;

    public Mono<ServerResponse> getAllUsers(ServerRequest request) {
        log.info(">>>> getAllUsers");
        return ServerResponse.ok().body(userService.getAllUsers(), UserDto.class);
    }

    public Mono<ServerResponse> saveUser(ServerRequest request) {
        log.info(">>>> getAllUsers");
        return request.bodyToMono(UserDto.class)
                .flatMap(userDto -> userService.saveUser(userDto)
                        .flatMap(user -> ServerResponse.ok().body(Mono.just("Ok"), String.class))
                        .switchIfEmpty(ServerResponse.badRequest().body(Mono.just("Empty"), String.class))
                )
                .onErrorResume((error) -> {
                    log.error(">>>> Error {}.", error.getMessage());
                    return ServerResponse.badRequest().body(Mono.just("Error: " + error.getMessage()), String.class);
                });

    }
}
