package com.webflux.postgres.services;

import com.webflux.postgres.application.dto.UserDto;
import com.webflux.postgres.application.mapper.UserMapper;
import com.webflux.postgres.persistences.postgres.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Mono<List<UserDto>> getAllUsers() {
        return Flux.fromIterable(userRepository.findAll()).map(user -> UserMapper.INSTANCE.convertEntityToDto(user)).collectList();
    }

    public Mono<UserDto> saveUser(final UserDto userDto) {
        return Mono.just(UserMapper.INSTANCE.convertEntityToDto(userRepository.save(UserMapper.INSTANCE.convertDtoToEntity(userDto))));
    }

}
