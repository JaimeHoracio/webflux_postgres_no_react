package com.webflux.postgres.persistences.postgres.entity;

public enum EnumRoles {

    USER,
    PARTNER,
    ADMIN
}
