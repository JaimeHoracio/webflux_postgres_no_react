package com.webflux.postgres.routers;

import com.webflux.postgres.handlers.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class UserRouter {

    @Bean
    public RouterFunction<ServerResponse> getUsers(final UserHandler userHandler){
        return RouterFunctions.route().GET("/api/users", userHandler::getAllUsers).build();
    }

    @Bean
    public RouterFunction<ServerResponse> saveUser(final UserHandler userHandler){
        return RouterFunctions.route().POST("/api/user", userHandler::saveUser).build();
    }

}
