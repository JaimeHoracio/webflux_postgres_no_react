package com.webflux.postgres.application.mapper;

import com.webflux.postgres.application.dto.UserDto;
import com.webflux.postgres.persistences.postgres.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto convertEntityToDto(final UserEntity entity);

    UserEntity convertDtoToEntity(final UserDto dto);

}
